import React from "react";
import Navbar from "./components/navbar";
import { Grid, Box } from "@material-ui/core";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./views/home";
import SignIn from "./views/signIn";
import SignUp from "./views/signUp";
import Cart from "./views/cart";
const App = () => {
  return (
    <Router>
      <Grid container direction="column">
        <Grid item>
          <Navbar />
        </Grid>
        <Box my={8}>
          <Grid item container>
            <Grid item sm={2}></Grid>
            <Grid item sm={8}>
              <Switch>
                <Route path="/" exact component={Home} />
                <Route path="/SignIn" exact component={SignIn} />
                <Route path="/SignUp" exact component={SignUp} />
                <Route path="/Cart" exact component={Cart} />
              </Switch>
            </Grid>
            <Grid item sm={2}></Grid>
          </Grid>
        </Box>
      </Grid>
    </Router>
  );
};

export default App;
